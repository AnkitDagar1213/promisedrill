const fs = require("fs");
const { v4: uuidv4 } = require("uuid");

function readPromise(filePath) {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, "utf8", (error, data) => {
      if (error) {
        reject(error);
      } else {
        resolve(data);
      }
    });
  });
}

function writePromise(filePath, data) {
  return new Promise((resolve, reject) => {
    fs.writeFile(filePath, data, "utf8", (error) => {
      if (error) {
        reject(error);
      } else {
        resolve("Written Successfully");
      }
    });
  });
}

function chmodPromise(filePath, mode) {
  return new Promise((resolve, reject) => {
    fs.chmod(filePath, mode, (error) => {
      if (error) {
        reject(error);
      } else {
        resolve("Permission changed");
      }
    });
  });
}

function accessPromise(filePath, mode) {
  return new Promise((resolve, reject) => {
    fs.access(filePath, mode, (error) => {
      if (error) {
        reject(error);
      } else {
        resolve("Permission accessed");
      }
    });
  });
}

readPromise("readfile.txt")
  .then((data) => {
    console.log("Data: " + data);
    return data;
  })
  .catch((error) => {
    console.error(error);
  })
  .then((data) => {
    console.log("Uppercase:", data.toUpperCase());
    return data.toUpperCase();
  })
  .catch((error) => {
    console.error(error);
  })
  .then((data) => {
    return writePromise("writefile.txt", data);
  })
  .catch((error) => {
    console.log(error);
  })
  .then((data) => {
    console.log(data);
  })
  .catch((error) => {
    console.error(error);
  });

writePromise("uuid.txt", uuidv4())
  .then((data) => {
    console.log(data);
    return chmodPromise("./uuid.txt", 0o777);
  })
  .catch((error) => {
    console.error(error);
  })
  .then((data) => {
    console.log(data);
  })
  .catch((error) => {
    console.log(error);
  })
  .then(() => {
    return accessPromise("uuid.txt", fs.constants.W_OK);
  })
  .catch((error) => {
    console.log(error);
  })
  .then((data) => {
    console.log(data);
  })
  .catch((error) => {
    console.error(error);
  });
